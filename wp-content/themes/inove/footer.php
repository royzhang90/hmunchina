	</div>
	<!-- main END -->

	<?php
		$options = get_option('inove_options');
		global $inove_nosidebar;
		if(!$options['nosidebar'] && !$inove_nosidebar) {
			get_sidebar();
		}
	?>
	<div class="fixed"></div>
</div>
<!-- content END -->

<!-- footer START -->
<div id="footer" style="background:none;">
	<div id="copyright" style="color:#858585">©2009. All Rights Reserved. Harvard Model United Nations.
           <span style="float:right;width:210px"><a style="color:#858585" href="http://www.blueluna.com" target="_blank">Boston Web Design</a> : <a style="color:#858585" href="http://www.blueluna.com" target="_blank">BlueLuna</a></span>
         </div>
</div>
<!-- footer END -->

</div>
<!-- container END -->
</div>
<!-- wrap END -->

<?php
	wp_footer();

	$options = get_option('inove_options');
	if ($options['analytics']) {
		echo($options['analytics_content']);
	}
?>

</body>
</html>

